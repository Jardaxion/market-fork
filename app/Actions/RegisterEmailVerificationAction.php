<?php

namespace App\Actions;

class RegisterEmailVerificationAction
{
    private $code;

    public function __construct(){
        $this->code = random_int(1111, 9999);
        session(['code' => $this->code]);
    }
    /**
     * @param string $email
     * @return bool
     */
   public function sendEmail(string $email):bool
   {
       \Illuminate\Support\Facades\Mail::to($email)->send(new \App\Mail\RegisterEmailVerification($this->code));

       return true;

   }
}
