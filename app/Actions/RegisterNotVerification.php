<?php

namespace App\Actions;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RegisterNotVerification
{
    public function __construct(){
        User::whereNull('email_verified_at')->delete();
    }
}
