<?php

namespace App\Http\Requests\Auth;

use App\Rules\PasswordConfirmation;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $emailRule = new EmailRequest();
        $nickName = new NicknameRequest();
        $password = new PasswordRequest();
        $password_confirmation = new PasswordConfirmed();

        return [
            'email' => Arr::get($emailRule->rules(),'email'),
            'password' => Arr::get($password->rules(),'password'),
            'password_confirmation' => Arr::get($password_confirmation->rules(),'password_confirmation'),
            'nickname' => Arr::get($nickName->rules(),'nickname'),
        ];
    }
}
