<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class PasswordConfirmation implements ValidationRule
{
    public $referredField = null;

    public function __construct($referredField){
        $this->referredField = $referredField;
    }
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        //

       if(request()->input( $this->referredField) !== $value){
           $fail('confirmation.error.password')->translate();
       }
    }
}
