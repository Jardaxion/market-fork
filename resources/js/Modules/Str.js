class str {
    static str_limit(text, count= 120, insertDots="..."){
        return text.slice(0, count) + (((text.length > count) && insertDots) ? "..." : "");
    }
}

export {str}
