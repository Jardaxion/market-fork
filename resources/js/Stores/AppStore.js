import {defineStore} from "pinia";

export const UseAppStore = defineStore('UseAppStore',{
    state: () => ({
        themeSite: localStorage.getItem('themeSite') || 'dark',
    }),
    actions:{
        toggleThemeSite() {
            this.themeSite === 'dark' ? this.themeSite = 'light' : this.themeSite = 'dark'
        }
    }
})
