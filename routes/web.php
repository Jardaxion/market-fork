<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/',fn() => Inertia::render('Main') )->name('main');

Route::get('privacy-policy',fn() => Inertia::render('Policy') )->name('privacy-policy');

Route::get('terms-of-use',fn() => Inertia::render('TermUse') )->name('terms-of-use');

Route::get('market',fn() => 1)->name('market');

Route::get('online-buy',fn() => 1)->name('online-buy');

Route::get('games',fn() => 1)->name('games');

Route::get('faq',fn() => Inertia::render('FAQ') )->name('faq');

Route::middleware(['auth','verified'])->prefix('dashboard')->group(function () {
    Route::get('/', fn() => Inertia::render('Dashboard'))->name('dashboard');
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
